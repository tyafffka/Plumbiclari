#ifndef _PLUMBICLARI_DATA_HPP
#define _PLUMBICLARI_DATA_HPP

#include <offnet-storage.hpp>
#include <vector>
#include <string>


/* Период показаний. */
struct t_period
{
    int64_t end_date;
    std::string toString();
    std::string toShortString();
};


/* Данные счётчика. */
struct t_plumbi
{
    std::string name;
    int64_t current_score;
    bool checked;
    bool paid;
    std::vector<int64_t> prev_scores;
};

struct t_plumbi2 : public t_plumbi
{
    int64_t price;
    int64_t prev_cost;
    std::string description;
};


/* Адаптер данных. */
class TData final
{
private:
    ons::connection storage;
    
    std::vector<std::string> plumbi_ids;
    
public:
    t_period current_period;
    std::vector<t_period> prev_periods;
    
    explicit TData(std::string const &storage_url_);
    void update();
    
    bool new_period(t_period period_);
    
    int new_plumbi();
    bool remove_plumbi(int i_);
    void load_plumbi(int i_, t_plumbi &plumbi_);
    void load_plumbi2(int i_, t_plumbi2 &plumbi_);
    bool save_plumbi(int i_, t_plumbi &plumbi_, bool=true /*not for using!*/);
    bool save_plumbi2(int i_, t_plumbi2 &plumbi_);
    
    int plumbis_count() {   return (int)plumbi_ids.size();   }
};


#endif // _PLUMBICLARI_DATA_HPP
