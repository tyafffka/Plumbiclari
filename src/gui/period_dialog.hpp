#ifndef _PLUMBICLARI_PERIOD_DIALOG_HPP
#define _PLUMBICLARI_PERIOD_DIALOG_HPP

#include "data.hpp"
#include "ui_period_dialog.h"
#include <QDialog>


class TPeriodDialog final : public QDialog, private Ui_period_dialog
{
    Q_OBJECT
    
private:
    t_period &__out_period;
    
public:
    explicit TPeriodDialog(t_period &out_period_, QWidget *parent=nullptr);
    
public Q_SLOTS:
    void accept();
};


#endif // _PLUMBICLARI_PERIOD_DIALOG_HPP
