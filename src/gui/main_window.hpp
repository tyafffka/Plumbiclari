#ifndef _PLUMBICLARY_MAIN_WINDOW_HPP
#define _PLUMBICLARY_MAIN_WINDOW_HPP

#include "data.hpp"
#include "ui_main_window.h"
#include <QMainWindow>


class TMainWindow final : public QMainWindow, private Ui_main_window
{
    Q_OBJECT
    
private:
    TData &data;
    bool __take_table;
    
public:
    explicit TMainWindow(TData &data_, QWidget *parent=nullptr);
    
public Q_SLOTS:
    void next_period();
    void table_select(int row_);
    void add_plumbi();
    void remove_plumbi();
    void edit_plumbi();
    
private Q_SLOTS:
    void __reload_row(int row_);
    void __reload_table();
    void __set_is_edit(bool is_);
};


#endif // _PLUMBICLARY_MAIN_WINDOW_HPP
