#include "data.hpp"
#include <time.h>

#define __SECONDS_IN_DAY (24 * 60 * 60)


static char const *__mounts[12] = {
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря"
};

static t_period __default_period = {14634}; // до 25 января 2010 года



std::string t_period::toString()
{
    time_t time__ = (time_t)end_date * __SECONDS_IN_DAY;
    struct tm *tm__ = gmtime(&time__);
    
    char buffer[60];
    snprintf(buffer, 60, "до %d %s %d года",
             tm__->tm_mday, __mounts[tm__->tm_mon], tm__->tm_year + 1900);
    return std::string(buffer);
}

std::string t_period::toShortString()
{
    time_t time__ = (time_t)end_date * __SECONDS_IN_DAY;
    struct tm *tm__ = gmtime(&time__);
    
    char buffer[10];
    snprintf(buffer, 10, "%02d.%02d", tm__->tm_mon + 1, tm__->tm_year % 100);
    return std::string(buffer);
}

// --- *** ---


TData::TData(std::string const &storage_url_) : storage(storage_url_)
{
    try
    {
        current_period = {(int64_t)storage.get("global", "current_period")};
    }
    catch(ons::not_found_exception &)
    {   current_period = __default_period;   }
    
    try
    {
        minson::array d_periods = storage.get("global", "periods");
        
        prev_periods.reserve(d_periods.count());
        for(auto d_period : d_periods)
            prev_periods.push_back(t_period{(int64_t)d_period});
    }
    catch(ons::not_found_exception &) {}
    
    minson::array plumbis_list = storage.getCategory("plumbi");
    
    plumbi_ids.reserve(plumbis_list.count());
    for(auto i : plumbis_list)
        plumbi_ids.push_back(i.name());
    return;
}


void TData::update()
{
    int16_t list_sz;
    ons::syncByList(storage, list_sz);
    
    storage.reset();
    return;
}


bool TData::new_period(t_period period_)
{
    if(! plumbi_ids.empty())
    {
        prev_periods.push_back(current_period);
        
        minson::array d_periods = minson::array("periods");
        for(t_period period : prev_periods)
            d_periods.putNumber(period.end_date);
        
        if(! storage.set("global", d_periods))
        {
            storage.reset(); return false;
        }
    }
    
    current_period = period_;
    
    if(! storage.set("global", minson::object::number(
        current_period.end_date, "current_period"
    )))
    {
        storage.reset(); return false;
    }
    
    // update all plumbis
    
    for(std::string id : plumbi_ids)
    {
        try
        {
            minson::array d_plumbi = storage.get("plumbi", id);
            minson::array d_plumbi2 = storage.get("plumbi2", id);
            
            if(d_plumbi.unnamedCount() > 0) // save previous cost
            {
                int64_t prev = 0;
                for(minson::iterator rit = d_plumbi.rbegin(), rend = d_plumbi.rend();
                    rit != rend; ++rit)
                {
                    if(! rit.hasName()) {   prev = (int64_t)*rit; break;   }
                }
                prev = ((int64_t)d_plumbi.get("current") - prev) * (int64_t)d_plumbi2.get("price");
                
                d_plumbi2.putNumber(prev, "prev_cost");
            }
            
            d_plumbi.putNumber((int64_t)d_plumbi.get("current")); // save previous score
            
            d_plumbi.putBoolean(false, "checked"); // reset flags
            d_plumbi.putBoolean(false, "paid");
            
            storage.set("plumbi", d_plumbi);
            storage.set("plumbi2", d_plumbi2);
        }
        catch(ons::not_found_exception &) {}
    }
    
    storage.flush();
    return true;
}

// --- *** ---


int TData::new_plumbi()
{
    std::string new_id = storage.generate("plumbi");
    if(new_id.empty())
    {
        storage.reset(); return -1;
    }
    if(! storage.set("plumbi2", minson::object::null(new_id)))
    {
        storage.reset(); return -1;
    }
    
    plumbi_ids.push_back(new_id);
    int i = (int)(plumbi_ids.size() - 1);
    
    storage.flush();
    return i;
}


bool TData::remove_plumbi(int i_)
{
    if(! storage.remove("plumbi", plumbi_ids[i_]) ||
       ! storage.remove("plumbi2", plumbi_ids[i_]))
    {
        storage.reset(); return false;
    }
    
    plumbi_ids.erase(plumbi_ids.cbegin() + i_);
    
    storage.flush();
    return true;
}


void TData::load_plumbi(int i_, t_plumbi &plumbi_)
{
    try
    {
        minson::array d_plumbi = storage.get("plumbi", plumbi_ids[i_]);
        
        plumbi_.name = (std::string)d_plumbi.get("name");
        plumbi_.current_score = (int64_t)d_plumbi.get("current");
        plumbi_.checked = (bool)d_plumbi.get("checked");
        plumbi_.paid = (bool)d_plumbi.get("paid");
        
        plumbi_.prev_scores.reserve(d_plumbi.unnamedCount());
        for(auto i : d_plumbi)
        {
            if(i.hasName()) continue;
            plumbi_.prev_scores.push_back((int64_t)i);
        }
        return;
    }
    catch(ons::not_found_exception &) {}
    catch(minson::cast_error &) {}
    
    // fallback:
    
    plumbi_.name = std::string();
    plumbi_.current_score = 0;
    plumbi_.checked = false;
    plumbi_.paid = false;
    plumbi_.prev_scores.assign(prev_periods.size(), 0);
    return;
}


void TData::load_plumbi2(int i_, t_plumbi2 &plumbi_)
{
    load_plumbi(i_, plumbi_);
    
    try
    {
        minson::array d_plumbi2 = storage.get("plumbi2", plumbi_ids[i_]);
        
        plumbi_.price = (int64_t)d_plumbi2.get("price");
        plumbi_.prev_cost = (int64_t)d_plumbi2.get("prev_cost");
        plumbi_.description = (std::string)d_plumbi2.get("description");
        return;
    }
    catch(ons::not_found_exception &) {}
    catch(minson::cast_error &) {}
    
    // fallback:
    
    plumbi_.price = 0;
    plumbi_.prev_cost = 0;
    plumbi_.description = std::string();
    return;
}


bool TData::save_plumbi(int i_, t_plumbi &plumbi_, bool is_flush__)
{
    minson::array d_plumbi = minson::array(plumbi_ids[i_]);
    d_plumbi.putString(plumbi_.name, "name");
    d_plumbi.putNumber(plumbi_.current_score, "current");
    d_plumbi.putBoolean(plumbi_.checked, "checked");
    d_plumbi.putBoolean(plumbi_.paid, "paid");
    
    for(int64_t score : plumbi_.prev_scores)
        d_plumbi.putNumber(score);
    
    if(! storage.set("plumbi", d_plumbi))
    {
        storage.reset(); return false;
    }
    
    if(is_flush__) storage.flush();
    return true;
}


bool TData::save_plumbi2(int i_, t_plumbi2 &plumbi_)
{
    if(! save_plumbi(i_, plumbi_, false)) return false;
    
    minson::array d_plumbi2 = minson::array(plumbi_ids[i_]);
    d_plumbi2.putNumber(plumbi_.price, "price");
    d_plumbi2.putNumber(plumbi_.prev_cost, "prev_cost");
    d_plumbi2.putString(plumbi_.description, "description");
    
    if(! storage.set("plumbi2", d_plumbi2))
    {
        storage.reset(); return false;
    }
    
    storage.flush();
    return true;
}
