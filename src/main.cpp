#include "data.hpp"
#include "gui/main_window.hpp"
#include <QApplication>
#include <QDir>
#include <iostream>


static int __fake_argc = 1;



int main(int argc, char **argv)
{
    std::string url;
    if(argc >= 2)
    {   url.assign(argv[1]);   }
    else
    {
        QString home = QDir::homePath();
        QDir homedir = QDir(home);
        homedir.mkpath(DEFAULT_HOME_LOCATION);
        
        url.assign("direct:");
        url.append(home.toStdString());
        url.append("/" DEFAULT_HOME_LOCATION);
    }
    
    try
    {
        QApplication app{__fake_argc, argv};
        
        TData data{url};
        data.update();
        
        TMainWindow main_window{data};
        return app.exec();
    }
    catch(ons::url_error &)
    {
        std::cerr << "Data connection errors!" << std::endl;
    }
    return 1;
}
