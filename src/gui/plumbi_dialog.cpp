#include "plumbi_dialog.hpp"
#include <math.h>



TPlumbiDialog::TPlumbiDialog(TData &data_, int entry_, QWidget *parent) :
    QDialog(parent), data(data_), __entry(entry_), __rewrited(false)
{
    data.load_plumbi2(entry_, __plumbi);
    
    setupUi(this);
    
    connect(edit_score, SIGNAL(valueChanged(int)),    this, SLOT(update_cost()));
    connect(edit_price, SIGNAL(valueChanged(double)), this, SLOT(update_cost()));
    connect(buttons, SIGNAL(clicked(QAbstractButton *)),
            this, SLOT(action(QAbstractButton *)));
    
    // table headers
    
    int p_count = (int)data.prev_periods.size();
    if(table_history->columnCount() < p_count)
        table_history->insertColumn(0);
    
    for(int i = 0; i < p_count; ++i)
    {
        std::string str_period = data.prev_periods[i].toShortString();
        table_history->setHorizontalHeaderItem(i, new QTableWidgetItem{
            QString::fromUtf8(str_period.c_str())
        });
    }
    
    // table scores
    
    int count = (int)__plumbi.prev_scores.size();
    for(int i = 0; i < count; ++i)
    {
        QString score = QString::number(__plumbi.prev_scores[i]);
        table_history->setItem(0, i, new QTableWidgetItem{score});
    }
    
    // rest data
    
    edit_name->setText(QString::fromUtf8(__plumbi.name.c_str()));
    edit_score->setValue((int)__plumbi.current_score);
    check_checked->setChecked(__plumbi.checked);
    edit_price->setValue(__plumbi.price / 100.0);
    view_prev_cost->setValue(__plumbi.prev_cost / 100.0);
    check_paid->setChecked(__plumbi.paid);
    edit_description->setPlainText(QString::fromUtf8(__plumbi.description.c_str()));
    
    //update_cost();
    return;
}


void TPlumbiDialog::reject()
{
    if(__rewrited)
        QDialog::accept();
    else
        QDialog::reject();
    return;
}


void TPlumbiDialog::action(QAbstractButton *button_)
{
    switch(buttons->standardButton(button_))
    {
    case QDialogButtonBox::Ok:
        save(); accept();
        return;
        
    case QDialogButtonBox::Save:
        save();
        return;
        
    case QDialogButtonBox::Cancel:
        reject();
        return;
        
    default: return;
    }
}


void TPlumbiDialog::update_cost()
{
    if(__plumbi.prev_scores.empty())
    {
        view_cost->setValue(.0); return;
    }
    
    int32_t score = (int32_t)edit_score->value();
    int32_t prev_score = (int32_t)__plumbi.prev_scores[__plumbi.prev_scores.size() - 1];
    double price = edit_price->value();
    
    view_cost->setValue((score - prev_score) * price);
    return;
}


void TPlumbiDialog::save()
{
    __plumbi.name = edit_name->text().toUtf8().toStdString();
    __plumbi.current_score = edit_score->value();
    __plumbi.checked = check_checked->isChecked();
    __plumbi.price = (int64_t)round(edit_price->value() * 100.0);
    __plumbi.paid = check_paid->isChecked();
    __plumbi.description = edit_description->toPlainText().toUtf8().toStdString();
    
    if(data.save_plumbi2(__entry, __plumbi))
        __rewrited = true;
    return;
}
