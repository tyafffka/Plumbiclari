#include "main_window.hpp"
#include "period_dialog.hpp"
#include "plumbi_dialog.hpp"
#include <QMessageBox>


static QColor __non_checked_color = QColor(210, 119, 119, 255);
static QColor __paid_color = QColor(153, 170, 137, 255);

static QString __remove_plumbi_title = QString::fromUtf8("Удаление счётчика");
static QString __remove_plumbi_message = QString::fromUtf8(
    "Вы хотите удалить всю информацию, связанную с выбранным счётчиком?"
);



TMainWindow::TMainWindow(TData &data_, QWidget *parent) :
    QMainWindow(parent), data(data_), __take_table(false)
{
    setupUi(this);
    setWindowTitle(windowTitle() + trUtf8(" (версия " PROJECT_VERSION ")"));
    
    connect(button_next_period,   SIGNAL(clicked()), this, SLOT(next_period()));
    connect(button_add_plumbi,    SIGNAL(clicked()), this, SLOT(add_plumbi()));
    connect(button_remove_plumbi, SIGNAL(clicked()), this, SLOT(remove_plumbi()));
    connect(button_edit_plumbi,   SIGNAL(clicked()), this, SLOT(edit_plumbi()));
    
    __reload_table();
    
    connect(table_plumbis, SIGNAL(currentCellChanged(int, int, int, int)),
            this, SLOT(table_select(int)));
    connect(table_plumbis, SIGNAL(cellDoubleClicked(int, int)),
            this, SLOT(edit_plumbi()));
    
    show();
    __take_table = true;
    return;
}


void TMainWindow::next_period()
{
    t_period period_buffer = data.current_period;
    if(TPeriodDialog(period_buffer, this).exec() != QDialog::Accepted)
        return;
    
    if(! data.new_period(period_buffer)) return;
    
    __take_table = false;
    {   __reload_table();   }
    __take_table = true;
    return;
}


void TMainWindow::table_select(int row_)
{
    if(! __take_table) return;
    
    __set_is_edit(0 <= row_ && row_ < data.plumbis_count());
    return;
}


void TMainWindow::add_plumbi()
{
    int row = data.new_plumbi();
    if(TPlumbiDialog(data, row, this).exec() != QDialog::Accepted)
    {
        data.remove_plumbi(row); return;
    }
    
    __take_table = false;
    {
        __reload_row(row);
        table_plumbis->setCurrentCell(
            row, 0, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows
        );
        __set_is_edit(true);
    }
    __take_table = true;
    return;
}


void TMainWindow::remove_plumbi()
{
    int row = table_plumbis->currentRow();
    if(row < 0 || data.plumbis_count() <= row) return;
    
    if(QMessageBox::question(
        this, __remove_plumbi_title, __remove_plumbi_message
    ) == QMessageBox::No) return;
    
    if(! data.remove_plumbi(row)) return;
    
    __take_table = false;
    {
        table_plumbis->removeRow(row);
        table_plumbis->clearSelection();
        __set_is_edit(false);
    }
    __take_table = true;
    return;
}


void TMainWindow::edit_plumbi()
{
    int row = table_plumbis->currentRow();
    if(row < 0 || data.plumbis_count() <= row) return;
    
    if(TPlumbiDialog(data, row, this).exec() != QDialog::Accepted)
        return;
    
    __take_table = false;
    {   __reload_row(row);   }
    __take_table = true;
    return;
}


void TMainWindow::__set_is_edit(bool is_)
{
    button_remove_plumbi->setEnabled(is_);
    button_edit_plumbi->setEnabled(is_);
    return;
}


void TMainWindow::__reload_row(int row_)
{
    if(row_ < 0) return;
    
    if(table_plumbis->rowCount() <= row_)
        table_plumbis->insertRow(row_);
    
    t_plumbi plumbi;
    data.load_plumbi(row_, plumbi);
    
    // name
    
    QTableWidgetItem *header = new QTableWidgetItem{
        QString::fromUtf8(plumbi.name.c_str())
    };
    if(! plumbi.checked)
        header->setBackgroundColor(__non_checked_color);
    else if(plumbi.paid)
        header->setBackgroundColor(__paid_color);
    
    table_plumbis->setVerticalHeaderItem(row_, header);
    
    // scores
    
    int count = (int)plumbi.prev_scores.size();
    for(int i = 0; i < count; ++i)
    {
        QString score = QString::number(plumbi.prev_scores[i]);
        table_plumbis->setItem(row_, i, new QTableWidgetItem{score});
    }
    
    QString score = QString::number(plumbi.current_score);
    table_plumbis->setItem(row_, count, new QTableWidgetItem{score});
    return;
}


void TMainWindow::__reload_table()
{
    // load period
    
    std::string str_period = data.current_period.toString();
    label_period->setText(QString::fromUtf8(str_period.c_str()));
    
    // load headers
    
    int p_count = (int)data.prev_periods.size();
    if(table_plumbis->columnCount() <= p_count)
        table_plumbis->insertColumn(0);
    
    for(int i = 0; i < p_count; ++i)
    {
        str_period = data.prev_periods[i].toShortString();
        table_plumbis->setHorizontalHeaderItem(i, new QTableWidgetItem{
            QString::fromUtf8(str_period.c_str())
        });
    }
    
    // load table
    
    int count = data.plumbis_count();
    for(int i = 0; i < count; ++i) __reload_row(i);
    
    while(table_plumbis->rowCount() > count)
        table_plumbis->removeRow(count);
    
    if(table_plumbis->rowCount() > 0)
    {
        table_plumbis->setCurrentCell(0, 0,
            QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows
        );
        __set_is_edit(true);
    }
    else
    {
        table_plumbis->clearSelection();
        __set_is_edit(false);
    }
    return;
}
