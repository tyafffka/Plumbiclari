#include "period_dialog.hpp"


static const QDate __epoch{1970, 1, 1};



TPeriodDialog::TPeriodDialog(t_period &out_period_, QWidget *parent) :
    QDialog(parent), __out_period(out_period_)
{
    setupUi(this);
    label_alert->hide();
    
    edit_period->setDate(__epoch.addDays(__out_period.end_date));
    return;
}


void TPeriodDialog::accept()
{
    int64_t new_date = __epoch.daysTo(edit_period->date());
    
    if(new_date <= __epoch.daysTo(QDate::currentDate()))
    {
        label_alert->show(); return;
    }
    
    __out_period.end_date = new_date;
    QDialog::accept();
    return;
}
