#ifndef _PLUMBICLARI_PLUMBI_DIALOG_HPP
#define _PLUMBICLARI_PLUMBI_DIALOG_HPP

#include "data.hpp"
#include "ui_plumbi_dialog.h"
#include <QDialog>


class TPlumbiDialog final : public QDialog, private Ui_plumbi_dialog
{
    Q_OBJECT
    
private:
    t_plumbi2 __plumbi;
    TData &data;
    int __entry;
    bool __rewrited;
    
public:
    explicit TPlumbiDialog(TData &data_, int entry_, QWidget *parent=nullptr);
    
public Q_SLOTS:
    void reject();
    void action(QAbstractButton *button_);
    void update_cost();
    void save();
};


#endif // _PLUMBICLARI_PLUMBI_DIALOG_HPP
